#! /bin/bash

# init_cairo_dock.sh --
#
#   This file initializes cairo-dock for Emmabuntüs distribution
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was checked and validated on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


clear


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_logiciel_affichage=

message_reset_dock="\n\
$(eval_gettext 'Do you want to reinstall the Emmabuntus default dock (Cairo-Dock) ?')\n\
\n\
\<span color=\'"'red\'"'> $(eval_gettext 'Warning:')\</span> $(eval_gettext 'Any changes or customization of the dock will be lost'),\n\
$(eval_gettext 'after the launch of this utility. The dock will return to its initial state defined during system install.')     "


export WINDOW_DIALOG_INIT_DOCK='<window title="'$(eval_gettext 'Reset Emmabuntus dock')'" icon-name="gtk-dialog-question" resizable="false">
<vbox spacing="0">

<text use-markup="true" wrap="false" xalign="0" justify="3">
<input>echo "'${message_reset_dock}'" | sed "s%\\\%%g"</input>
</text>

<hbox spacing="10" space-expand="false" space-fill="false">
<button cancel></button>
<button can-default="true" has-default="true" use-stock="true" is-focus="true">
<label>gtk-ok</label>
<action>exit:OK</action>
</button>
</hbox>

</vbox>
</window>'

MENU_DIALOG_INIT_DOCK="$(gtkdialog --center --program=WINDOW_DIALOG_INIT_DOCK)"

eval ${MENU_DIALOG_INIT_DOCK}
echo "MENU_DIALOG_INIT_DOCK=${MENU_DIALOG_INIT_DOCK}"


if [ ${EXIT} == "OK" ]
then

    # Suppression des docks supplémentaires lancés en mode de sauvegarde de session
    # [Deletion of all extra docks launched in saving session mode]

    pkill cairo-dock

    rm -R ~/.config/cairo-dock-language/
    rm -R ~/.config/cairo-dock

    unlink  ~/.config/cairo-dock

    /usr/bin/lock_cairo_dock.sh "Init"

    pkexec /usr/bin/init_cairo_dock_exec.sh ${USER}


    /usr/bin/start_cairo_dock.sh &

fi








