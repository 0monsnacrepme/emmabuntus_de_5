#! /bin/bash

# calamares_oem_postinstall_exec.sh --
#
#   This file permits to finalize OEM postinstall for the Emmabuntus Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

bureau=${XDG_DESKTOP_DIR}

file_config_settings="/etc/calamares/settings.conf"
file_config_finished="/etc/calamares/modules/finished.conf"
file_fstab="/etc/fstab"
wallpaper_file="/usr/share/xfce4/backdrops/emmabuntus_fond_ecran_ligthdm.jpg"

repertoire_emmabuntus=~/.config/emmabuntus
fichier_init=${repertoire_emmabuntus}/postinstall_calamares.txt

function FCT_WAIT_PROCESS()
{
sleep 2
while ps axg | grep -vw grep | grep -w $1 > /dev/null; do sleep 1; done
}

if [[ -f ${file_config_settings}.postoem ]] ; then

    DATE=`date +"%d:%m:%Y - %H:%M:%S"`

    echo "Lancement Post-Installation OEM " >> ${fichier_init}
    echo "DATE = $DATE" >> ${fichier_init}

    pkill cairo-dock
    pkill emmabuntus_config
    pkill gtkdialog

    if [[ -f "${bureau}/install-debian.desktop" ]] ; then
        rm "${bureau}/install-debian.desktop"
    fi

    if ps -A | grep "xfce4-session" ; then

        # Suppression des raccourcis
        xfconf-query -c xfce4-keyboard-shortcuts -p /commands/custom -r -R
        xfconf-query -c xfce4-keyboard-shortcuts -p /commands/default -r -R
        xfconf-query -c xfce4-keyboard-shortcuts -p /xfwm4/custom -r -R
        xfconf-query -c xfce4-keyboard-shortcuts -p /xfwm4/default -r -R

        # Désactivation des actions du desktop
        xfconf-query -c xfce4-desktop -p "/desktop-icons/style" -n -t int -s 0
        xfconf-query -c xfce4-desktop -p "/windowlist-menu/show" -n -t bool -s false
        xfconf-query -c xfce4-desktop -p "/desktop-menu/show" -n -t bool -s false

        # Maj fond d'écran
        xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/image-path" -s "${wallpaper_file}"
        xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/last-image" -s "${wallpaper_file}"
        xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/last-single-image" -s "${wallpaper_file}"
        xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/image-style" -s "5"

        xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace0/last-image" -s "${wallpaper_file}"
        xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace1/last-image" -s "${wallpaper_file}"
        xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace2/last-image" -s "${wallpaper_file}"
        xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace3/last-image" -s "${wallpaper_file}"

    fi

     if ps -A | grep "lxqt-session" ; then

        # Maj fond d'écran
         pcmanfm-qt --set-wallpaper ${wallpaper_file}
         pcmanfm-qt --wallpaper-mode stretch

    fi

    if ps -A | grep "xfce4-panel" ; then
        pkill xfce4-panel
    fi

    if ps -A | grep "lxqt-panel" ; then
        pkill lxqt-panel
        pkill lxqt-globalkeys
    fi

    if [[ -f ${file_config_settings} ]] ; then
        sudo rm ${file_config_settings}
    fi

    sudo cp ${file_config_settings}.postoem ${file_config_settings}

    if [[ -f ${file_config_finished} ]] ; then
        sudo rm ${file_config_finished}
    fi

    sudo cp ${file_config_finished}.postoem ${file_config_finished}

    # Mise en place du service pour supprimer le compte OEM

    # Si le fichier exsite suppression de celui-ci pour relancer le service
    if [[ -f /usr/lib/systemd/system/cleanupoem.service ]] ; then
        sudo rm /usr/lib/systemd/system/cleanupoem.service
    fi

    if [[ ! -f /usr/lib/systemd/system/cleanupoem.service ]] ; then
        sudo cp /opt/calamares/cleanupoem.service /usr/lib/systemd/system
    fi

    ls /usr/lib/systemd/system | grep cleanup.service >> ${fichier_init}

    # Lancement du service pour supprimer le compte OEM
    sudo systemctl enable cleanupoem

    sudo systemctl status cleanupoem 2>> ${fichier_init}

    # Lancement de Calamares
    sudo install-debian &

    for i in {1..30}
    do

        DATE=`date +"%d:%m:%Y - %H:%M:%S"`
        wmctrl -l >> ${fichier_init}
        echo "${i} - DATE = $DATE" >> ${fichier_init}

        sleep 1

        if [[ -f "${bureau}/install-debian.desktop" ]] ; then
            rm "${bureau}/install-debian.desktop"
        fi

        if ps -A | grep "xfce4-panel" ; then
            pkill xfce4-panel
        fi

        if ps -A | grep "lxqt-panel" ; then
            pkill lxqt-panel
            pkill lxqt-globalkeys
        fi

        # Patch non remise en place du fichier fstab par Calamares
        if [[ (! -f ${file_fstab}) && (-f ${file_fstab}.orig.calamares) ]] ; then
            sudo mv ${file_fstab}.orig.calamares ${file_fstab}
        fi

        #wmctrl -a Emmabuntüs -b add,fullscreen

    done

    FCT_WAIT_PROCESS install-debian

    # Patch non remise en place du fichier fstab par Calamares
    if [[ (! -f ${file_fstab}) && (-f ${file_fstab}.orig.calamares) ]] ; then
        sudo mv ${file_fstab}.orig.calamares ${file_fstab}
    fi

    systemctl -i reboot

    exit 0

else

    exit 1

fi


