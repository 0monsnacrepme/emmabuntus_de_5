<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>show</name>
    <message>
        <location filename="../show.qml" line="45"/>
        <source>Welcome to Debian 12 GNU/Linux - Emmabuntus DE 5.&lt;br/&gt;The rest of the installation is automated and should complete in a few minutes.</source>
        <translation>Bienvenue dans Debian 12 GNU/Linux - Emmabuntüs DE 5.&lt;br/&gt;Le reste de l'installation est automatisé et devrait s'achever dans quelques minutes.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="67"/>
        <source>More customization available through the post-installation process.</source>
        <translation>Plus de personnalisation grâce à la post-installation.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="88"/>
        <source>More accessibility for everyone, thanks to the three dock levels !</source>
        <translation>Plus d'accessibilité grâce à 3 niveaux de dock pour tous !</translation>
    </message>
    <message>
        <location filename="../show.qml" line="109"/>
        <source>More than 60 software programs available, which allow the beginners to discover GNU/Linux.</source>
        <translation>Plus de 60 logiciels présents pour permettre aux débutants la découverte de GNU/Linux.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="130"/>
        <source>More than 10 edutainment software programs for training, including Kiwix the offline Wikipedia reader.</source>
        <translation>Plus de 10 logiciels ludo-éducatifs pour la formation, dont Kiwix le lecteur de Wikipédia hors-ligne.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="151"/>
        <source>Emmabuntus supports social and environmental projects through the Lilo ethical and solidarity research engine.</source>
        <translation>Grâce au moteur de recherche éthique et solidaire Lilo, Emmabuntüs soutient des projets sociaux et environnementaux.</translation>
    </message>
        <message>
        <location filename="../show.qml" line="171"/>
        <source>Emmabuntus collaborates with more than 10 associations, particularly in Africa, in order to reduce the digital divide.</source>
        <translation>Emmabuntüs collabore avec plus de 10 associations, en particulier en Afrique, pour réduire la fracture numérique.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="191"/>
        <source>Emmabuntus together with YovoTogo and JUMP Lab'Orione have equipped and run 32 computer rooms in Togo.</source>
        <translation>Emmabuntüs, YovoTogo et JUMP Lab'Orione ont équipé et animent ensemble 32 salles informatiques au Togo.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="211"/>
        <source>Emmabuntus is also a refurbishing key, based on Ventoy,&lt;br/&gt;allowing you to quickly and easily refurbish a computer with GNU/Linux.</source>
        <translation>Emmabuntüs c'est aussi une clé USB de réemploi basée sur Ventoy&lt;br/&gt;permettant de reconditionner facilement et rapidement un ordinateur avec GNU/Linux.</translation>
    </message>
</context>
</TS>
