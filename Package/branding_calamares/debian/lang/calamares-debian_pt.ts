<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt">
<context>
    <name>show</name>
    <message>
        <location filename="../show.qml" line="45"/>
        <source>Welcome to Debian 12 GNU/Linux - Emmabuntus DE 5.&lt;br/&gt;The rest of the installation is automated and should complete in a few minutes.</source>
        <translation>Bem-vindo ao Debian 12 GNU/Linux - Emmabuntus DE 5.&lt;br/&gt;O restante da instalação é automatizado e deve ser concluído em alguns minutos.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="67"/>
        <source>More customization available through the post-installation process.</source>
        <translation>Mais personalização disponível através do processo de pós-instalação.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="88"/>
        <source>More accessibility for everyone, thanks to the three dock levels !</source>
        <translation>Mais acessibilidade para todos, graças aos três níveis de doca!</translation>
    </message>
    <message>
        <location filename="../show.qml" line="109"/>
        <source>More than 60 software programs available, which allow the beginners to discover GNU/Linux.</source>
        <translation>Mais de 60 programas de software disponíveis, que permitem aos iniciantes descobrir o GNU/Linux.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="130"/>
        <source>More than 10 edutainment software programs for training, including Kiwix the offline Wikipedia reader.</source>
        <translation>Mais de 10 programas de software educativo para treinamento, incluindo Kiwix, o leitor offline da Wikipédia.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="151"/>
        <source>Emmabuntus supports social and environmental projects through the Lilo ethical and solidarity research engine.</source>
        <translation>Emmabuntüs apoia projetos sociais e ambientais através do motor de pesquisa ética e solidária de Lilo.</translation>
    </message>
        <message>
        <location filename="../show.qml" line="171"/>
        <source>Emmabuntus collaborates with more than 10 associations, particularly in Africa, in order to reduce the digital divide.</source>
        <translation>Emmabuntüs colabora com mais de 10 associações, particularmente na África, para reduzir o fosso digital.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="191"/>
        <source>Emmabuntus together with YovoTogo and JUMP Lab'Orione have equipped and run 32 computer rooms in Togo.</source>
        <translation>Emmabuntüs, juntamente com a YovoTogo e a JUMP Lab'Orione, equiparam e administraram 32 salas de computadores no Togo.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="211"/>
        <source>Emmabuntus is also a refurbishing key, based on Ventoy,&lt;br/&gt;allowing you to quickly and easily refurbish a computer with GNU/Linux.</source>
        <translation>Emmabuntus também é uma chave de recondicionamento, baseada no Ventoy,&lt;br/&gt;permitindo que você recondicione um computador com GNU/Linux de maneira rápida e fácil.</translation>
    </message>
</context>
</TS>
